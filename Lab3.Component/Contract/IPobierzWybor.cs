﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Implementation;

namespace Lab3.Contract
{
    public interface IPobierzWybor
    {
        IPobierzWybor Metoda(Sterowanie s);
        void Pobierz(int przycisk);
        void Wybierz(int przycisk);
    }
}
