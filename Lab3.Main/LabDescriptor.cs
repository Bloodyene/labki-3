﻿using System;
using Lab3.Implementation;
using Lab3.Contract;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IPobierzWybor);
        public static Type I2 = typeof(IUruchom);
        public static Type I3 = typeof(IUstawCzas);

        public static Type Component = typeof(Sterowanie);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Roz);
        public static Type MixinFor = typeof(Sterowanie);

        #endregion
    }
}
